import { ArcRotateCamera, Color3, FreeCamera, HemisphericLight, MeshBuilder, SceneLoader, StandardMaterial, Vector3 } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
import { GameState } from "../lib/game/GameState";
import "@babylonjs/loaders/glTF"

export class ModelsSampleLevel extends BaseLevel
{

    camera:FreeCamera;

    constructor()
    {
        super();
        const light = new HemisphericLight("light", new Vector3(0,1,0), this.scene);
        light.intensity = 0.5;
        
        //3D Object
        const ground = MeshBuilder.CreateGround("ground", {width: 10, height:10}, this.scene);
        const sphereball = MeshBuilder.CreateSphere("sphereball", {diameter:1}, this.scene);
        sphereball.position = new Vector3(0,1,0)

        this.cube = MeshBuilder.CreateBox("cube",{size:1}, this.scene);
        this.cube.position.x = 2;
        this.cube.position.y = 1;
        this.cube.position.z =-2;
        this.cube.material
        
        const cubeMat = new StandardMaterial("cubeMaterial", this.scene);
        cubeMat.diffuseColor = Color3.FromHexString("#FF0000");

        this.cube.material = cubeMat;
        // t.material.di
        // this.cube.material.wireframe=true;//  .diffuseColor = Color3.FromHexString("#404655");
        
        //camera overwritten from freeCamera to ArcRotateCamera
        // const arcCamera = new ArcRotateCamera('camera', 0, 0, 10, new Vector3(0, 0, 0), this.scene);
        //  arcCamera.attachControl()

        this.camera = new FreeCamera("camera", new Vector3(0,1,-10), this.scene);
        this.camera.attachControl();
    //  (<ArcRotateCamera>this.camera).setTarget( this.cube.position );
     
    //         const guiMenu = AdvancedDynamicTexture.CreateFullscreenUI("UI");
    // guiMenu.idealHeight = 720; //fit our fullscreen ui to this height

    this.createBarrel();

    
    

     
        this.state = GameState.PLAYING;
    }



    update(delta?:number, args?:any[]): void 
    {
        switch( this.state )
            {
                case GameState.LOADING:
                    break;

                case GameState.PLAYING:
                    this.cube.rotation.x+=.02;
                        break;    
            }
       
    }//

    render(): void 
    {
       
            switch( this.state )
            {
                case GameState.LOADING:
                    break;

                case GameState.PLAYING:
                            this.scene.render();
                        break;    
            }

    }//


    createBarrel():void
    {

        //if this .gltf is used we have to update the names of the textures
        // "/assets/barrel_03/","barrel_03.gltf"

        //if .glb used ( from blender export ) nothing has to be updated

        // SceneLoader.ImportMesh("","/assets/barrel_03/","barrel_03.glb", 
        SceneLoader.ImportMesh("","/assets/barrel_03/","barrel_03.gltf", 
        this.scene,(meshes)=>{
            // console.log("some resp here",  );
            console.log("meshes: ", meshes );
        })
    }

}