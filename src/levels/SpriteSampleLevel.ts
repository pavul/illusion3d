import { ArcRotateCamera, AssetsManager, Camera, Color3, Color4, CubeTexture, Engine, FreeCamera, HemisphericLight, Mesh, MeshBuilder, PointLight, Sprite, SpriteManager, StandardMaterial, TextFileAssetTask, Texture, Vector3, Vector4 } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
import { GameState } from "../lib/game/GameState";


export class SpriteSampleLevel extends BaseLevel
{

    camera:FreeCamera;
    // camera:ArcRotateCamera;
    ship:Sprite;

    constructor()
    {
        super();
        const light = new HemisphericLight("light", new Vector3(0,1,0), this.scene);
        light.intensity = 0.5;
        
        const shipMngr = new SpriteManager("shipMngr", "/assets/ship.png", 1, { width:21 , height:12 }, this.scene );
     
        this.ship = new Sprite("ship", shipMngr);
        this.ship.cellIndex = 1;
        this.ship.width=.5;
        this.ship.height=.5;
        // this.ship.width=.6;
                // this.ship.height=.4;


        this.camera = new FreeCamera("camera", new Vector3(0,0,-5), this.scene);
        this.camera.attachControl();
        // this.camera.setTarget( new Vector3(0,0,0))

        // const camera = new ArcRotateCamera("Camera", -Math.PI / 2, Math.PI / 2, 8, new Vector3(0, 0, -4), this.scene);
        // camera.attachControl();
        
        // Configurar la cámara como ortográfica
        this.camera.mode = Camera.ORTHOGRAPHIC_CAMERA;

        // Configurar el tamaño de la ventana de la cámara ortográfica
        this.camera.orthoTop = 5;
        this.camera.orthoBottom = -5;
        this.camera.orthoLeft = -5;
        this.camera.orthoRight = 5;

        
        // const light = new PointLight("Point", new Vector3(5, 10, 5), this.scene);
    

        // const skybox = MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, this.scene);
        // const skyboxMaterial = new StandardMaterial("skyBox", this.scene);
        // skyboxMaterial.backFaceCulling = false;
        // skyboxMaterial.reflectionTexture = new CubeTexture("/assets/skybox/skybox", this.scene);
        // skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;
        // skyboxMaterial.diffuseColor = new Color3(1,1,1);
        // skyboxMaterial.specularColor = new Color3(1, 0, 0);
        // skybox.material = skyboxMaterial;


        //#TILES 1 WORKS
        // const tiles = MeshBuilder.CreateGround( 
        //         "tiles",{width:100, height:100, subdivisionsX:200, subdivisionsY:100},this.scene );
        
        // const groundMaterial = new StandardMaterial('groundMaterial', this.scene);
        // // groundMaterial.wireframe = true; // Habilita el wireframe
        // tiles.material = groundMaterial;
        // tiles.setAbsolutePosition( new Vector3(0,0,0) )
        
        // //rotate the tile mesh horizontally
        // tiles.rotation.x = Math.PI / 2;
        
        // //put tiles behind
        // tiles.position.z = 1;


        //#TILES2
        const tiledPlane = MeshBuilder.CreateTiledPlane(
            'tiledPlane',
            {
              pattern: Mesh.CENTER,
              size: 100, // Tamaño de un "tile" en unidades
              width: 100, // Ancho total del TiledPlane en unidades (3 "tiles" de 16 unidades cada uno)
              height: 10, // Alto del TiledPlane en unidades (1 "tile" de 16 unidades)
              tileWidth:.5,
              tileHeight:1
            },
            this.scene
          )
          
          tiledPlane.position.z=1;

        const groundMaterial = new StandardMaterial('groundMaterial', this.scene);
        groundMaterial.wireframe = true; // Habilita el wireframe
        tiledPlane.material = groundMaterial;

        this.scene.clearColor = new Color4(0,0,0,1)



    //     let spriteSheet = 
    //     new Texture("textures/spriteMap/none_trimmed/Legends_Level_A.png", this.scene,
    //     false, //NoMipMaps
    //     false, //InvertY usually false if exported from TexturePacker
    //     Texture.NEAREST_NEAREST, //Sampling Mode
    //     null, //Onload, you could spin up the sprite map in a function nested here
    //     null, //OnError
    //     null, //CustomBuffer
    //     false, //DeleteBuffer
    //     Engine.TEXTURETYPE_UNSIGNED_INT//ImageFormageType RGBA
    // );

    // console.log( "spriteSheet:" );
    // console.log( spriteSheet );


    // const assetsManager = new AssetsManager(this.scene);
    // const textTask = assetsManager.addTextFileTask("text task", "textures/spriteMap/none_trimmed/Legends_Level_A.json");



        this.state = GameState.PLAYING;
    }



    update(delta?:number, args?:any[]): void 
    {

        switch( this.state )
            {
                case GameState.LOADING:
                    break;

                case GameState.PLAYING:
                    
                        break;    
            }
       
    }//

    render(): void 
    {
       
            switch( this.state )
            {
                case GameState.LOADING:
                    break;

                case GameState.PLAYING:
                            this.scene.render();
                        break;    
            }

    }//


}