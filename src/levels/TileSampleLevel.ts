import { ArcRotateCamera, AssetsManager, Camera, Color3, Color4, CubeTexture, Engine, FreeCamera, HemisphericLight, KeyboardEventTypes, Mesh, MeshBuilder, PointLight, Sprite, SpriteManager, SpriteMap, SpritePackedManager, StandardMaterial, TextFileAssetTask, Texture, Vector2, Vector3, Vector4 } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
import { GameState } from "../lib/game/GameState";
import { Initiable } from "../lib/ntfc/Initiable";
import { MathUtil } from "../lib/util/MathUtil"
import  * as GUI from '@babylonjs/gui';
import { Game } from "../lib/game/Game";

export class TileSampleLevel extends BaseLevel
    implements Initiable
{

    readonly SHIP_SPD:number = 0.04;
    readonly STAR_NBR:number = 15;
    readonly LSR_CNTR:number = 5;

    camera:FreeCamera;
    // camera:ArcRotateCamera;
    ship:Sprite;
    shipCollider:Mesh;


    moveHor:number=0;
    moveVer:number=0;

    stars:Mesh[]=[];
    lasers:Mesh[]=[];

    advancedTexture:GUI.AdvancedDynamicTexture;// = GUI.AdvancedDynamicTexture.CreateFullscreenUI('UI');
    textBlock:GUI.TextBlock;// = new GUI.TextBlock();


    // let listItemSelected = undefined;
    
    enemies2:Sprite[] = []
    ene2Colliders:Mesh[]=[];

    enemies3:Sprite[] = []
    ene3Colliders:Mesh[]=[];



    // bullets:Sprite[] = []
    // eneBulletCollider
    readonly MAX_BULLETS:number = 10;
    bullets:Mesh[]=[];
    bullet:Sprite;

    score:number=0;

    constructor()
    {
        super();
        const light = new HemisphericLight("light", new Vector3(0,0,-4), this.scene);
        light.intensity = 1;
        
        const shipMngr = new SpriteManager("shipMngr", "/assets/ship.png", 1, { width:21 , height:12 }, this.scene );
     
        this.ship = new Sprite("ship", shipMngr);
        this.ship.cellIndex = 1;
        // this.ship.width=.5;
        // this.ship.height=.5;
        this.ship.width=.6;
        this.ship.height=.4;


        // this.camera = new FreeCamera("camera", new Vector3(0,0,-5), this.scene);
        // this.camera.attachControl();
        // this.camera.setTarget( new Vector3(0,0,0))

        // const camera = new ArcRotateCamera("Camera", -Math.PI / 2, Math.PI / 2, 8, new Vector3(0, 0, -4), this.scene);
        // camera.attachControl();
        
        this.camera = new FreeCamera('orthoCamera', new Vector3(0, 0, -10), this.scene);
        this.camera.attachControl();
        this.camera.mode = Camera.ORTHOGRAPHIC_CAMERA;
        
        // Establecer la relación de aspecto de la cámara a 16:9
        this.camera.orthoTop = 5;
        this.camera.orthoBottom = -5;
        this.camera.orthoLeft = -8;
        this.camera.orthoRight = 8;

        this.scene.clearColor = new Color4(0,0,0,1)

    }

    async init(): Promise<void> 
    {

                let tilesetImg = 
                new Texture("/assets/tiles/atlas.png", this.scene,
                false, //NoMipMaps
                false, //InvertY usually false if exported from TexturePacker
                Texture.NEAREST_NEAREST, //Sampling Mode
                null, //Onload, you could spin up the sprite map in a function nested here
                null, //OnError
                null, //CustomBuffer
                false, //DeleteBuffer
                Engine.TEXTURETYPE_UNSIGNED_INT//ImageFormageType RGBA
                );

          
                
            //getting data from tilred.json which contains the frames in to be used by spriteMap
            const tilemapJson = await this.loadData();
            tilemapJson.layers[0].frames.push()
            // console.log("tilemapJson")
            // console.log(tilemapJson.layers[0].frames)

            //size of the level or the size ( in colums and rows ) of the grid of tiles 
            const tilemapSize:Vector2 = new Vector2(100,10);

            /**
             * accepts
             * name of the object
             * the frames in json format to use
             * the texture that contain the tiles
             */
            const tileMap = new SpriteMap("tileMap", {"frames":tilemapJson.layers[0].frames}, tilesetImg,
            {
                stageSize: tilemapSize,
                flipU: true
            }, this.scene);

            //accomodating the x position because sprite map will be put at center 
            tileMap.position._x = 42;
            tileMap.position._z = 2;

            const frames:any[] = tilemapJson.layers[0].frames;

            // frames[74].frame.w=16;
            // frames[74].width=.05
            // frames[74].height=.05
            console.log("Frame 74: ", frames[74])
            

            // console.log("array final: ", jsonArray)
            for(let idx:number =0; idx < frames.length; idx++ )
            // for( const frame of tilemapJson.layers[0].frames)
            {
                const frame = frames[idx];
                frame.frame.width=8;
                frame.frame.heigth=8;
                
                tileMap.changeTiles(0, new Vector2(frame.position.x, frame.position.y), idx)    
            }
    

          

             //set the tiles here for every map
            //  this.scene.onKeyboardObservable.add((kbInfo) => {
            //     switch (kbInfo.type) 
            //     {
            //         case KeyboardEventTypes.KEYDOWN:
            //             switch (kbInfo.event.key) 
            //             {
            //                 case "a":
            //                 case "A":
            //                     this.moveHor = -this.SHIP_SPD;
            //                 break;

            //                 case "d":
            //                 case "D":
            //                     this.moveHor = this.SHIP_SPD;
            //                 break;

            //                 case "w":
            //                 case "W":
            //                     this.moveVer = this.SHIP_SPD;
            //                     this.ship.cellIndex=0;
            //                 break;

            //                 case "s":
            //                 case "S":
            //                     this.moveVer = -this.SHIP_SPD;
            //                     this.ship.cellIndex=2;
            //                 break;
            //             }
            //         break;
            //         case KeyboardEventTypes.KEYUP:
            //             switch(kbInfo.event.key)
            //             {
            //                 case "a":
            //                 case "A":
            //                 case "d":
            //                 case "D":
            //                 case "w":
            //                 case "W":
            //                 case "s":
            //                 case "S":
            //                 this.moveHor = 0;
            //                 this.moveVer = 0;
            //                 this.ship.cellIndex = 1;
            //                 break
            //                 case " ":
            //                     console.log("fire bullet")
            //                     for( let idx:number =0; idx<this.LSR_CNTR ;idx++)
            //                     {
            //                         console.log(`shoot laser:${idx} - alpha ${this.lasers[idx].material.alpha} - pos: ${this.lasers[idx].position}`, )
            //                         if( !this.lasers[idx].isVisible )
            //                         {
            //                             // this.lasers[idx].position.x = ;
            //                             // this.lasers[idx].position.y = ;

            //                             this.lasers[idx].position = new Vector3(this.ship.position.x, this.ship.position.y, 0);

            //                             this.lasers[idx].isVisible=true;
            //                             break;
            //                         }
            //                     }
            //                 break;
            //             }
            //         break;
            //     }
               
            // });

            //create text to show in screen
         this.advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI('UI');
         this.textBlock = new GUI.TextBlock();
        
         this.textBlock.color = 'white';
         this.textBlock.fontSize = 24;
         this.textBlock.top = '-400px'; // Cambiar la posición vertical
         this.textBlock.left = '-800px'; // Cambiar la posición horizontal
         this.advancedTexture.addControl(this.textBlock);


            this.state = GameState.PLAYING;
    }//init


    async loadData():Promise<any>
    {
        const assetsManager = new AssetsManager(this.scene);
        const jsonData = await new Promise( (res:any, rej:any)=>{
          const jsonTilemapData = assetsManager.addTextFileTask("jsonTilemapData", "/assets/tiles/tilered.json");
    
            jsonTilemapData.onSuccess = (task)=>
            {
                res(JSON.parse(task.text));
            };
            assetsManager.load();

        }) 
        
        console.log("returning data: ", jsonData)
        return jsonData;
    }

    async update(delta?:number, args?:any[]): Promise<void> 
    {

        switch( this.state )
            {
                case GameState.LOADING:
                    await this.init()
                    break;

                case GameState.PLAYING:
                
                break;    
            }
       
    }//

    render(): void 
    {
       
            switch( this.state )
            {
                case GameState.LOADING:
                    break;

                case GameState.PLAYING:
                            this.scene.render();
                        break;    
            }

    }//


    // displayText()
    // {
    //     const advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI('UI');
    //     const textBlock = new GUI.TextBlock();
    //     textBlock.text = `FPS: ${Game.fpsCounter}` ;
    //     textBlock.color = 'white';
    //     textBlock.fontSize = 24;
    //     textBlock.top = '0px'; // Cambiar la posición vertical
    //     textBlock.left = '0px'; // Cambiar la posición horizontal
    //     advancedTexture.addControl(textBlock);
        
    // }


}