

/**
 * here is where the game starts this is the Game App entry point
 */

import { ModelsSampleLevel } from "./levels/ModelsSampleLevel";
import { SampleLevel } from "./levels/SampleLevel";
import { Shooter2DsampleLevel } from "./levels/Shooter2DsampleLevel";
import { SpriteSampleLevel } from "./levels/SpriteSampleLevel";
import { TileSampleLevel } from "./levels/TileSampleLevel";
import { Game } from "./lib/game/Game";
import { BaseLevel } from "./lib/level/BaseLevel";

const game = new Game();
// game.loadLevel( new SampleLevel() );
// game.loadLevel( new SpriteSampleLevel() );
// game.loadLevel( new TileSampleLevel() );

// game.loadLevel( new TileSampleLevel() );
game.loadLevel( new Shooter2DsampleLevel() );

// game.loadLevel( new ModelsSampleLevel() );



game.runWithBabylonEngine =true;
game.calculateFps=true;
game.targetFPS=75;
window.onload = function(){game.run();} 

window.addEventListener("resize", function () {
    Game.engine.resize();
});