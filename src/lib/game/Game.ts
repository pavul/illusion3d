

import {Engine, Scene} from '@babylonjs/core';
import { LevelLoadable } from '../ntfc/LevelLoadable';
import { BaseLevel } from '../level/BaseLevel';
import { Renderable } from '../ntfc/Renderable';
import { Runnable } from '../ntfc/Runnable';


export class Game
    implements LevelLoadable, Renderable, Runnable
{

    
    static engine:Engine;
    currentLevel:BaseLevel;

    delta: number;
    lastUpdate: number;
    targetFPS:number;
    millisPerFrame:number; // this is 1000 / fps
    calculateFps:boolean;

    runWithBabylonEngine:boolean;

    //to calculate fps
    framesCount:number=0;
    frames:number = 0;
    static fps:number = 0;
    lastFPSUpdate:number = Date.now();

    constructor()
    {

        const canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("canvas"); 
        Game.engine=new Engine(canvas,true);

        this.delta = 0;
        this.lastUpdate = 0;
        this.targetFPS = 60;// 1000/30;  //30 fps, use 1000/60 to set it to 60 fps
        this.millisPerFrame = 1/this.targetFPS;

        //below are tho show and set FPS counter
        this.calculateFps = false;
        this.runWithBabylonEngine = false;
    }

    
    /**
     * this will start the render loop.
     * this function has two different game loops one custom and another
     * managed by runRenderLoop of babylon, runWithBabylonEngine is set to false
     * wich means the game loop to run by default is the customized one, but if 
     * you want to use babylon loop set that variable to true before execute run method.
     */
    run()
    {

        if( !this.runWithBabylonEngine )
        {
                let now = Date.now();
                // this.delta = now - this.lastUpdate;
                this.delta = ( now - this.lastUpdate ) / 1000; //this will give like 0.016 which is ok for delta
              
                if( this.delta >= 1 / this.targetFPS )
                {
                    if( this.currentLevel )
                    {
                        this.currentLevel.update( this.delta );
                        this.currentLevel.render( );
                        this.lastUpdate = now;
                        this.frames++;
                    }

                }

                if( this.calculateFps )
                {
                    if (now - this.lastFPSUpdate >= 1000) 
                    {
                        
                        Game.fps =this.frames;
                        this.frames = 0;
                        this.lastFPSUpdate = now;
                        console.log("FPS: ", Game.fps)
                    }
                }
            
            requestAnimationFrame( this.run.bind(this) );
        }
        else
        {
            Game.engine.runRenderLoop(()=>{
                if( this.currentLevel )
                {
                    this.currentLevel.update();
                    this.currentLevel.render();
                }
            });
        }
    }


    // updateFpsCounter()
    // {
    //     const fff = Math.ceil( 1/this.delta );
    //     // console.log("FFF:", fff)
    //         Game.fpsCounter = fff
    // }


    
    loadLevel(level: BaseLevel): boolean 
    {
        this.currentLevel = level
        return true;
    }


    render(): void {

    }

    

  

}