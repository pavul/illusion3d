import { Sprite } from "@babylonjs/core/Sprites";


/**
 * this class is a singleton, you have to getInstance first, 
 * 
 */
export class SpriteCollisionUtil
{

    static instance:SpriteCollisionUtil;

    static getInstance()
    {
        if( this.instance === undefined || this.instance === null )
        { 
            this.instance = new SpriteCollisionUtil();
        }
        return this.instance;
        
    }

    /**
     * check collision between 2 sprites
     * @param sprite1 
     * @param sprite2 
     * @returns 
     */
    spriteCollision(sprite1: Sprite, sprite2: Sprite): boolean {
        const left1 = sprite1.position.x - sprite1.width / 2;
        const right1 = sprite1.position.x + sprite1.width / 2;
        const top1 = sprite1.position.y - sprite1.height / 2;
        const bottom1 = sprite1.position.y + sprite1.height / 2;
    
        const left2 = sprite2.position.x - sprite2.width / 2;
        const right2 = sprite2.position.x + sprite2.width / 2;
        const top2 = sprite2.position.y - sprite2.height / 2;
        const bottom2 = sprite2.position.y + sprite2.height / 2;
    
        return ( left1 < right2 && right1 > left2 && top1 < bottom2 && bottom1 > top2 );
    }


    spriteSideCollision(sprite1: Sprite, sprite2: Sprite): string | null {
        const left1 = sprite1.position.x - sprite1.width / 2;
        const right1 = sprite1.position.x + sprite1.width / 2;
        const top1 = sprite1.position.y - sprite1.height / 2;
        const bottom1 = sprite1.position.y + sprite1.height / 2;
    
        const left2 = sprite2.position.x - sprite2.width / 2;
        const right2 = sprite2.position.x + sprite2.width / 2;
        const top2 = sprite2.position.y - sprite2.height / 2;
        const bottom2 = sprite2.position.y + sprite2.height / 2;
    
        // Calcula la distancia entre los centros de los Sprites en ambos ejes
        const deltaX = sprite1.position.x - sprite2.position.x;
        const deltaY = sprite1.position.y - sprite2.position.y;
    
        const combinedHalfWidths = (sprite1.width + sprite2.width) / 2;
        const combinedHalfHeights = (sprite1.height + sprite2.height) / 2;
    
        // Verifica de qué lado se produce la colisión
        if (Math.abs(deltaX) <= combinedHalfWidths && Math.abs(deltaY) <= combinedHalfHeights) 
        {
            const overlapX = combinedHalfWidths - Math.abs(deltaX);
            const overlapY = combinedHalfHeights - Math.abs(deltaY);
    
            if (overlapX >= overlapY) 
            {
                // Colisión vertical (arriba o abajo)
                if (deltaY > 0) {
                    return "top"; // up collision
                } else {
                    return "bottom"; // down collision
                }
            } else {
                // Colisión horizontal (izquierda o derecha)
                if (deltaX > 0) 
                {
                    return "left"; //left collision
                } else {
                    return "right"; //right collision
                }
            }
        }
    
        return undefined; // No collision
    }


}