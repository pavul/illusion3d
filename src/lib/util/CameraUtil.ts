import { Camera } from "@babylonjs/core";
import { OrthographicCameraOptions } from "../camera/OrthographicCameraOptions";



export class CameraUtil
{


    setOrthographic( cam:any, options?:OrthographicCameraOptions)
    {
        if( options )
        {
            cam.orthoTop = options.top;
            cam.orthoBottom = options.bottom;
            cam.orthoLeft = options.left;
            cam.orthoRight = options.right;
        }
        else
        {
            cam.mode = Camera.ORTHOGRAPHIC_CAMERA;
            // Configurar el tamaño de la ventana de la cámara ortográfica
            cam.orthoTop = 5;
            cam.orthoBottom = -5;
            cam.orthoLeft = -5;
            cam.orthoRight = 5;
        }
        
    }


}