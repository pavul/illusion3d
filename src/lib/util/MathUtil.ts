


export class MathUtil
{

    /**
     * returns random number between min and max
     * @param min 
     * @param max 
     * @returns 
     */
    static randomNumber(min:number, max:number)
    {
        if (min == max)
			return (min);

        const random = Math.random();
		return ((random * (max - min)) + min);
		
    }
    





}