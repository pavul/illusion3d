import { BaseLevel } from "../level/BaseLevel";


export interface LevelLoadable
{


    loadLevel(level:BaseLevel):boolean;

}