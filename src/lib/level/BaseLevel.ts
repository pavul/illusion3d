import { Camera, FreeCamera, Mesh, Scene, Vector3 } from "@babylonjs/core";
import { Updatable } from "../ntfc/Updatable";
import { Renderable } from "../ntfc/Renderable";
import { Game } from "../game/Game";
import { GameState } from "../game/GameState";


export class BaseLevel
    implements Updatable, Renderable
{
  
    protected scene:Scene;
    // protected  camera:Camera;
    protected state:GameState;

    cube:Mesh;
    

    constructor()
    {
        this.state = GameState.LOADING;
        this.scene = new Scene(Game.engine);
    }


    update(delta?:number, args?:any[]): void {}

    render(): void {}//


}