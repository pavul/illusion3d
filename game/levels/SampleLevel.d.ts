import { FreeCamera } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
export declare class SampleLevel extends BaseLevel {
    camera: FreeCamera;
    constructor();
    update(delta?: number, args?: any[]): void;
    render(): void;
}
