import { FreeCamera, Sprite } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
export declare class SpriteSampleLevel extends BaseLevel {
    camera: FreeCamera;
    ship: Sprite;
    constructor();
    update(delta?: number, args?: any[]): void;
    render(): void;
}
