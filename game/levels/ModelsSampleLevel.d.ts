import { FreeCamera } from "@babylonjs/core";
import { BaseLevel } from "../lib/level/BaseLevel";
import "@babylonjs/loaders/glTF";
export declare class ModelsSampleLevel extends BaseLevel {
    camera: FreeCamera;
    constructor();
    update(delta?: number, args?: any[]): void;
    render(): void;
    createBarrel(): void;
}
