import { Engine } from '@babylonjs/core';
import { LevelLoadable } from '../ntfc/LevelLoadable';
import { BaseLevel } from '../level/BaseLevel';
import { Renderable } from '../ntfc/Renderable';
import { Runnable } from '../ntfc/Runnable';
export declare class Game implements LevelLoadable, Renderable, Runnable {
    static engine: Engine;
    currentLevel: BaseLevel;
    delta: number;
    lastUpdate: number;
    targetFPS: number;
    millisPerFrame: number;
    calculateFps: boolean;
    runWithBabylonEngine: boolean;
    framesCount: number;
    frames: number;
    static fps: number;
    lastFPSUpdate: number;
    constructor();
    /**
     * this will start the render loop.
     * this function has two different game loops one custom and another
     * managed by runRenderLoop of babylon, runWithBabylonEngine is set to false
     * wich means the game loop to run by default is the customized one, but if
     * you want to use babylon loop set that variable to true before execute run method.
     */
    run(): void;
    loadLevel(level: BaseLevel): boolean;
    render(): void;
}
