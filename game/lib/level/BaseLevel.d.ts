import { Mesh, Scene } from "@babylonjs/core";
import { Updatable } from "../ntfc/Updatable";
import { Renderable } from "../ntfc/Renderable";
import { GameState } from "../game/GameState";
export declare class BaseLevel implements Updatable, Renderable {
    protected scene: Scene;
    protected state: GameState;
    cube: Mesh;
    constructor();
    update(delta?: number, args?: any[]): void;
    render(): void;
}
