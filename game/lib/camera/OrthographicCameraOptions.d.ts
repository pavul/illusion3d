/**
 * this interface should be used when we wan't our camera to be orthographic settings
 */
export interface OrthographicCameraOptions {
    left: number;
    right: number;
    bottom: number;
    top: number;
}
