import { OrthographicCameraOptions } from "../camera/OrthographicCameraOptions";
export declare class CameraUtil {
    setOrthographic(cam: any, options?: OrthographicCameraOptions): void;
}
