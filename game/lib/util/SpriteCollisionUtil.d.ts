import { Sprite } from "@babylonjs/core/Sprites";
/**
 * this class is a singleton, you have to getInstance first,
 *
 */
export declare class SpriteCollisionUtil {
    static instance: SpriteCollisionUtil;
    static getInstance(): SpriteCollisionUtil;
    /**
     * check collision between 2 sprites
     * @param sprite1
     * @param sprite2
     * @returns
     */
    spriteCollision(sprite1: Sprite, sprite2: Sprite): boolean;
    spriteSideCollision(sprite1: Sprite, sprite2: Sprite): string | null;
}
