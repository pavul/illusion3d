export declare class MathUtil {
    /**
     * returns random number between min and max
     * @param min
     * @param max
     * @returns
     */
    static randomNumber(min: number, max: number): number;
}
